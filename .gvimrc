set columns=230
set lines=70

" No toolbar
set guioptions-=T

" No menubar
set guioptions-=m

set guifont=Monospace\ 11

" Colors
" colorscheme jellybeans
" set background=light
" set background=light
" colorscheme solarized
" let g:solarized_contrast = "high"

" Install plugins with vim-plug
" (run `:PlugInstall` on first run)
call plug#begin('~/.vim/plugged')

Plug 'rking/ag.vim'
Plug 'kien/ctrlp.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'lambdatoast/elm.vim'
Plug 'mattn/emmet-vim'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround'
Plug 'vimwiki/vimwiki'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'ultimatecoder/goyo-doc'
Plug 'pearofducks/ansible-vim'
Plug 'sotte/presenting.vim'
Plug 'sickill/vim-monokai'
Plug 'leafgarland/typescript-vim'
Plug 'ianks/vim-tsx'
Plug 'Quramy/tsuquyomi'
Plug 'tpope/vim-obsession'
Plug 'sk1418/HowMuch'
Plug 'morhetz/gruvbox'
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': 'yarn install --frozen-lockfile'}
Plug 'altercation/vim-colors-solarized'
Plug 'puremourning/vimspector'
Plug 'jparise/vim-graphql'
Plug 'christoomey/vim-system-copy'

" Initialize plugin system
call plug#end()

" This holds the vim-plug commands
runtime plugins.vim

syntax on
set encoding=utf-8

" case insensitive search
set ignorecase

" text width and colorcolumn
set tw=0
set colorcolumn=0

" Pathogen
silent! execute pathogen#infect()
silent! Helptags

" Reload .vimrc on save
autocmd! bufwritepost .vimrc source %

" Use spaces not tabs
set expandtab

" No vi
set nocompatible

" Set changed buffers as hidden
" set hidden

set laststatus=2

" Set leader character
let mapleader = ','

" Signcolumn and numbers
nnoremap <leader>nsc :set nonumber<cr>:set scl=no<cr>
nnoremap <leader>sc :set number<cr>:set scl=yes<cr>

" Git
nnoremap <leader>gs :Gstatus<cr> " Open file under cursor with `D`
nnoremap <leader>gb :Gblame<cr>
nnoremap <leader>gl :Glog 
nnoremap <leader>glf :Glog -- %<cr> 
nnoremap <leader>gr :Grebase -i 

" ag
if executable('ag')
  nnoremap <C-k> :Ag 
  set runtimepath^=~/.vim/bundle/ag
endif

" CtrlP
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPMRU'
let g:ctrlp_working_path_mode = 'r'
let g:ctrlp_max_files=0
let g:ctrlp_max_depth=40
let g:ctrlp_custom_ignore = {
  \ 'dir': '\v[\/]node_modules$',
  \ }
" Speed up CtrlP scanning:
let g:ctrlp_cache_dir = $HOME . '.cache/ctrlp'
if executable('ag')
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

" edit/source .vimrc
nnoremap <leader>ev :tabf $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" flake8
nnoremap <leader>f8 :!flake8 %<cr>

" nerdtree
nnoremap <leader>nt :NERDTree<cr>
nnoremap <leader>nc :NERDTreeClose<cr>
nnoremap <leader>nf :NERDTreeFind<cr>

" paste/nopaste
nnoremap <leader>p :set paste<cr>
nnoremap <leader>np :set nopaste<cr>

" textwidth
" nnoremap <leader>t :set tw=79<cr>:set colorcolumn=80<cr>
" nnoremap <leader>nt :set tw=0<cr>:set colorcolumn=0<cr>

" prettify json with python
nnoremap <leader>jp :%!python -m json.tool<cr>:setf javascript<cr>

" gofmt 
nnoremap <leader>gf :%!gofmt %<cr>

" Map tab left/right
nnoremap <leader>l <esc>:e .<CR>

" Spell checking
nnoremap <leader>se :setlocal spell spelllang=en_us<cr>
nnoremap <leader>sd :setlocal spell spelllang=de_de<cr>

" Change background
nnoremap <leader>bd :set bg=dark<cr>
nnoremap <leader>bl :set bg=light<cr>

" vim airline
let g:airline_powerline_fonts = 1

" Sorting
vnoremap <leader>s :sort<CR>

" Set foldmethod on indent set foldlevel to a 
" larger value, so folds are open by default
set foldmethod=indent
set foldlevel=99

" Tab smartness
set smarttab

" 1 tab = 4 spaces
set ts=4
set shiftwidth=4

" Show current position
set ruler

set autoindent
set lbr

" Line numbers
set number

" Change to current files folder
set autochdir

" Show buffer title
set title

" Read folder specific .vimrc files:
set exrc

" Colors
" set termguicolors
" if $TERM == 'xterm-256color'
"     set t_Co=256
" endif

set t_Co=256
syntax enable
set background=light
" colorscheme desert
" let g:solarized_termcolors=256
" colorscheme desert
colorscheme solarized
" let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
" let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" No backup
set nobackup
set nowb
set noswapfile

" Toggle taglist (requires exuberant-ctags)
map <C-b> :TagbarToggle<Enter>

" ctags
set tags=./tags,tags;$HOME
let g:easytags_dynamic_files=1

" Set 5 lines from cursor to top or bottom when moving with j/k
" set so=5

" Search highlighting
set hlsearch

" Command bar hight
" set cmdheight=1

:filetype plugin on
filetype on

" custom file type highlighting
au BufNewFile,BufRead *.html set filetype=html
au BufNewFile,BufRead *.ftl set filetype=html
au BufNewFile,BufRead *.vm set filetype=html
au BufNewFile,BufRead *.md set filetype=markdown
au BufNewFile,BufRead *.json set filetype=json
au BufNewFile,BufRead *.plantuml set filetype=plantuml
au BufNewFile,BufRead *.pu set filetype=plantuml
au BufNewFile,BufRead *.uml set filetype=plantuml
au BufNewFile,BufRead *.conf set filetype=config

" vimwiki
nnoremap <leader>wb :VimwikiAll2HTML<cr>
let g:vimwiki_list = [{
  \ 'path': '~/vimwiki/',
  \ 'template_path': 'templates',
  \ 'template_default': 'default',
  \ 'path_html': 'html/',
  \ 'css_name': 'app.css',
  \ 'template_ext': '.html'},{
  \ 'path': '~/vimwiki-work/',
  \ 'template_path': 'templates',
  \ 'template_default': 'default',
  \ 'path_html': 'html/',
  \ 'css_name': 'app.css',
  \ 'template_ext': '.html'},]
let g:vimwiki_folding = 'list'

" vim-bookmarks
nmap <Leader>mm <Plug>BookmarkToggle
nmap <Leader>mi <Plug>BookmarkAnnotate
nmap <Leader>ma <Plug>BookmarkShowAll
nmap <Leader>mj <Plug>BookmarkNext
nmap <Leader>mk <Plug>BookmarkPrev
nmap <Leader>mc <Plug>BookmarkClear
nmap <Leader>mx <Plug>BookmarkClearAll
nmap <Leader>mkk <Plug>BookmarkMoveUp
nmap <Leader>mjj <Plug>BookmarkMoveDown
nmap <Leader>rn <Plug>(coc-rename)

" Diff
set diffopt+=vertical

" Goyo
" nmap <Leader>gg :Goyo<Enter>
" nmap <Leader>gd :Goyo 100x100%<Enter>:set number<Enter>

" Limelight
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

"Nvim settings
if has('nvim')
    :tnoremap <Esc> <C-\><C-n>
endif

" Coc.nvim
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
" if has("patch-8.1.1564")
"   " Recently vim can merge signcolumn and number column into one
"   set signcolumn=number
" else
"   set signcolumn=yes
" endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" NeoVim-only mapping for visual mode scroll
" Useful on signatureHelp after jump placeholder of snippet expansion
if has('nvim')
  vnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#nvim_scroll(1, 1) : "\<C-f>"
  vnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#nvim_scroll(0, 1) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" Mappings for CoCExplorer
:nnoremap <space>e :CocCommand explorer<CR>

" vimspector
let g:vimspector_test_plugin_path = expand( '<sfile>:p:h:h' )
let g:vimspector_enable_mappings = 'HUMAN'
let &rtp = &rtp . ',' . g:vimspector_test_plugin_path


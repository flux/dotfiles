VIM_PLUG_URL=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
VIM_PLUG_INSTALL_PATH=$HOME/.vim/autoload/plug.vim
VIM_PLUGIN_FILE_PATH=$HOME/.vim/plugins.vim
DOTFILES_DIR=$HOME/dotfiles

# Download vim-plug
if [ ! -f "$VIM_PLUG_INSTALL_PATH" ]; then
  curl -fLo $VIM_PLUG_INSTALL_PATH --create-dirs \
      $VIM_PLUG_URL
fi

# Put plugins file into vim runtimepath
if [ ! -f "$HOME/.vim/plugins.vim" ]; then
    cd .vim && ln -s $DOTFILES_DIR/plugins.vim ; cd -
fi

# Install plugins using only the plugins file, so all plugins
# Are loaded when vim is fired up with the regular .vimrc file
# for the first time.
vim -u $HOME/.vim/plugins.vim +PlugInstall +qall

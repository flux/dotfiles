#!/usr/bin/bash

PLUGINS=(
    tmux-resurrect,https://github.com/tmux-plugins/tmux-resurrect
)

echo Adding tmux plugins
echo "#################"

# Create directories if necessary
mkdir -p $HOME/.tmux/plugins

# Clone plugin directories
for plugin in ${PLUGINS[@]}; do
    # Set field separator
    OLDIFS=$IFS; IFS=','

    set -- $plugin

    PLUGIN_NAME=$1
    REPO_URL=$2
    PLUGIN_DIR="$HOME/.tmux/plugins/$PLUGIN_NAME"

    # Reset field separator
    IFS=$OLDIFS

    if [ -d "$PLUGIN_DIR" ]; then
        echo "Directory $PLUGIN_DIR already exists"
    else
        git clone $REPO_URL $PLUGIN_DIR
    fi
done

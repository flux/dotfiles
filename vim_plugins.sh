#!/usr/bin/bash

PLUGINS=(
    ag.vim,https://github.com/rking/ag.vim.git
    ctrlp.vim,https://github.com/kien/ctrlp.vim.git
    editorconfig-vim,https://github.com/editorconfig/editorconfig-vim.git
    elm.vim,https://github.com/lambdatoast/elm.vim.git
    emmet-vim,https://github.com/mattn/emmet-vim.git
    nerdtree,https://github.com/scrooloose/nerdtree.git
    vim-airline,https://github.com/vim-airline/vim-airline.git
    vim-fugitive,https://github.com/tpope/vim-fugitive.git
    vim-gitgutter,https://github.com/airblade/vim-gitgutter.git
    vim-surround,https://github.com/tpope/vim-surround.git
    vimwiki,https://github.com/vimwiki/vimwiki.git
    vim-bookmarks,https://github.com/MattesGroeger/vim-bookmarks.git
    goyo.vim,https://github.com/junegunn/goyo.vim.git
    limelight.vim,https://github.com/junegunn/limelight.vim.git
    goyo-doc,https://github.com/ultimatecoder/goyo-doc.git
    ansible-vim,https://github.com/pearofducks/ansible-vim.git
    presenting.vim,https://github.com/sotte/presenting.vim.git
    vim-monokai,https://github.com/sickill/vim-monokai.git
    typescript-vim,https://github.com/leafgarland/typescript-vim.git
    vim-tsx,https://github.com/ianks/vim-tsx.git
    tsuquyomi,https://github.com/Quramy/tsuquyomi.git
    vim-obession,https://github.com/tpope/vim-obsession.git
    howmuch,https://github.com/sk1418/HowMuch
    gruvbox,https://github.com/morhetz/gruvbox
    coc,https://github.com/neoclide/coc.nvim
    solarized,https://github.com/altercation/vim-colors-solarized
    vimspector,https://github.com/puremourning/vimspector
    vim-graphql,https://github.com/jparise/vim-graphql
    vim-system-copy,https://github.com/christoomey/vim-system-copy
)

echo Adding vim plugins
echo "#################"

# Create directories if necessary
mkdir -p $HOME/.vim/autoload $HOME/.vim/bundle

# Get vim-pathogen
curl https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim -o $HOME/.vim/autoload/pathogen.vim

# Clone plugin directories
for plugin in ${PLUGINS[@]}; do
    # Set field separator
    OLDIFS=$IFS; IFS=','

    set -- $plugin

    PLUGIN_NAME=$1
    REPO_URL=$2
    PLUGIN_DIR="$HOME/.vim/bundle/$PLUGIN_NAME"

    # Reset field separator
    IFS=$OLDIFS

    if [ -d "$PLUGIN_DIR" ]; then
        echo "Directory $PLUGIN_DIR already exists"
    else
        git clone $REPO_URL $PLUGIN_DIR
    fi
done

# Install coc-vim
cd ~/.vim/bundle/coc && yarn install && yarn build

# Dotfiles

Clone this repo from your home dir.

## vim 

```
# Run this from your home dir:
ln -s dotfiles/.vimrc
bash dotfiles/install-plugins.sh
```

## tmux

```
# Run this from your home dir:
ln -s dotfiles/.tmux.conf
```
